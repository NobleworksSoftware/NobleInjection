/*
 * Copyright (C) 2015, Dale King
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nobleworks_software.injection;

import dagger.MembersInjector;

/**
 * A very efficient key map lookup for a fixed set of data using double-hashed open addressing for
 * a known quantity of data.
 *
 * This implementation does not support deleting and does not check if you exceed the size allocated
 * for it in which case it will simply hang on put.
 */
public class ClassInjectorMap
{
    /**
     * The class instances that are the keys of the map
     */
    private final Class<?>[] keys;

    /**
     * The member injectors that are the values of the map
     */
    private final MembersInjector<?>[] values;

    /**
     * Creates a map of the given size. The size needs to be prime and the number of items that
     * will be put into the map should not be larger than 70% of this size.
     *
     * So if the number of items we knew we want to put in is n, the easy way to compute the size is:
     *
     * <code>BigInteger.valueOf(n * 10L / 7L).nextProbablePrime().intValue()</code>
     *
     * Do not be scared by the word "probable", that really only applies to huge numbers that are
     * larger than int.
     *
     * @param size The size of the map which needs to be prime and ~43% greater than the number of
     *             items that will go in the map.
     */
    @SuppressWarnings("unchecked")
    public ClassInjectorMap(int size)
    {
        keys = new Class<?>[size];
        values = new MembersInjector<?>[size];
    }

    /**
     * Find the index in the arrays for the given key where the key either exists or an empty slot
     * where it can be inserted.
     *
     * @param key The key to lookup in the map
     * @return The index where the key currently exists or where it can be inserted
     */
    private int lookup(Class<?> key)
    {
        int hash = key.hashCode();
        int index;
        Class<?> entry;

        // Loop through the arrays, probing to find the right spot
        do
        {
            // Compute a new hash on each time through the loop to achieve double hashing
            hash = hash * 57 + 43;

            // Use the hash to compute an index in the array
            index = Math.abs(hash % keys.length);
            entry = keys[index];
        }
        // We are done when we find an empty slot or the key we are looking for
        while(entry != null && entry != key);

        return index;
    }

    /**
     * Get the value for the given key or null if the key is not in the map
     *
     * @param key The key to look up in the map
     * @return The value for the key or null if the key is not in the map.
     */
    public MembersInjector<?> get(Class<?> key)
    {
        return values[lookup(key)];
    }

    /**
     * Put the given value into the map for the given key
     *
     * @param key The key to store the value for
     * @param value The value to store for the key
     */
    public void put(Class<?> key, MembersInjector<?> value)
    {
        // Find the location where to store it, either an empty slot
        // or the current location of the key
        int index = lookup(key);

        keys[index] = key;
        values[index] = value;
    }
}

