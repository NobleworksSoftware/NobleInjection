package com.nobleworks_software.injection;

public interface GenericMembersInjector
{
    /**
     * Perform member injection on the given instance. If there is no injection method defined
     * for the object's class or any of its superclasses throws an
     * <code>IllegalArgumentException</code>
     *
     * @param instance Object to be injected
     * @param <T> The type of the object
     * @throws IllegalArgumentException if no injection is defined for the class or any of its
     *      superclasses
     */
    <T> void injectMembers(T instance);
}
