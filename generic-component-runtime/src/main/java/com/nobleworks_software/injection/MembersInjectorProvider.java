package com.nobleworks_software.injection;

import dagger.MembersInjector;

/**
 * Interface for a type that can provide a member injector generically based on a target object.
 */
public interface MembersInjectorProvider
{
    /**
     * Get a member injector for the given object if one is defined for the given object
     *
     * @param instance The object to retrieve a <code>MembersInjector</code> for
     * @param <T> The type of the object
     * @return A <code>MembersInjector</code> instance to inject the given object or null if
     *     no injection is defined for the given class
     */
    <T> MembersInjector<? super T> getInjector(T instance);
}
