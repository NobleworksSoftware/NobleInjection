package frogermcs.io.githubclient.presenters.impl;

import frogermcs.io.githubclient.presenters.Presenter;

public class BasePresenter<T> implements Presenter<T>
{
    protected T view;

    @Override
    public void bindView(T view)
    {
        this.view = view;
    }
}
