package frogermcs.io.githubclient.presenters;

import frogermcs.io.githubclient.views.RepositoryDetailsView;

public interface RepositoryDetailsViewPresenter extends Presenter<RepositoryDetailsView>
{
}
