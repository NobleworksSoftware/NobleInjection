package frogermcs.io.githubclient.presenters.impl;

import com.google.common.collect.ImmutableList;

import javax.inject.Inject;

import frogermcs.io.githubclient.data.api.RepositoriesManager;
import frogermcs.io.githubclient.data.model.Repository;
import frogermcs.io.githubclient.presenters.RepositoriesListViewPresenter;
import frogermcs.io.githubclient.views.RepositoryListView;
import frogermcs.io.githubclient.utils.SimpleObserver;

/**
 * Created by Miroslaw Stanek on 23.04.15.
 */
public class RepositoriesListViewPresenterImpl extends BasePresenter<RepositoryListView>
    implements RepositoriesListViewPresenter
{
    private RepositoriesManager repositoriesManager;

    @Inject
    public RepositoriesListViewPresenterImpl(RepositoriesManager repositoriesManager)
    {
        this.repositoriesManager = repositoriesManager;
    }

    public void loadRepositories()
    {
        view.showLoading(true);
        repositoriesManager.getUsersRepositories().subscribe(new SimpleObserver<ImmutableList<Repository>>() {
            @Override
            public void onNext(ImmutableList<Repository> repositories) {
                view.showLoading(false);
                view.setRepositories(repositories);
            }

            @Override
            public void onError(Throwable e)
            {
                view.showLoading(false);
            }
        });
    }

}
