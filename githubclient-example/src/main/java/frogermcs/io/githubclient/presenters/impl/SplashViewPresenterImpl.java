package frogermcs.io.githubclient.presenters.impl;

import javax.inject.Inject;

import frogermcs.io.githubclient.data.api.UserManager;
import frogermcs.io.githubclient.data.model.User;
import frogermcs.io.githubclient.presenters.SplashViewPresenter;
import frogermcs.io.githubclient.views.SplashView;
import frogermcs.io.githubclient.utils.SimpleObserver;
import frogermcs.io.githubclient.utils.Validator;

public class SplashViewPresenterImpl extends BasePresenter<SplashView> implements SplashViewPresenter
{
    public CharSequence userName;

    private Validator validator;
    private UserManager userManager;

    @Inject
    public SplashViewPresenterImpl(Validator validator, UserManager userManager)
    {
        this.validator = validator;
        this.userManager = userManager;
    }

    public void setUserName(CharSequence username)
    {
        this.userName = username;
    }

    @Override
    public void showRepositories()
    {
        if (validator.validUsername(userName))
        {
            view.showLoading(true);

            userManager.getUser(userName).subscribe(new SimpleObserver<User>()
            {
                @Override
                public void onNext(User user)
                {
                    view.showLoading(false);
                    view.showRepositoriesListForUser(user);
                }

                @Override
                public void onError(Throwable e)
                {
                    view.showLoading(false);
                    view.showValidationError();
                }
            });
        }
        else
        {
            view.showValidationError();
        }
    }
}
