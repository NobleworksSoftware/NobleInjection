package frogermcs.io.githubclient.presenters;

import frogermcs.io.githubclient.views.RepositoryListView;

/**
 * Created by Miroslaw Stanek on 23.04.15.
 */
public interface RepositoriesListViewPresenter extends Presenter<RepositoryListView>
{
    void loadRepositories();
}
