package frogermcs.io.githubclient.presenters.impl;

import javax.inject.Inject;

import frogermcs.io.githubclient.data.model.User;
import frogermcs.io.githubclient.presenters.RepositoryDetailsViewPresenter;
import frogermcs.io.githubclient.views.RepositoryDetailsView;

/**
 * Created by Miroslaw Stanek on 23.04.15.
 */
public class RepositoryDetailsViewPresenterImpl extends BasePresenter<RepositoryDetailsView>
    implements RepositoryDetailsViewPresenter
{
    User user;

    @Inject
    public RepositoryDetailsViewPresenterImpl(User user)
    {
        this.user = user;
    }

    @Override
    public void bindView(RepositoryDetailsView view)
    {
        super.bindView(view);

        view.setUserName(user.login);
    }
}
