package frogermcs.io.githubclient.presenters;

import frogermcs.io.githubclient.views.SplashView;

public interface SplashViewPresenter extends Presenter<SplashView>
{
    void showRepositories();

    void setUserName(CharSequence userName);
}
