package frogermcs.io.githubclient.presenters;

public interface Presenter<T>
{
    void bindView(T view);
}
