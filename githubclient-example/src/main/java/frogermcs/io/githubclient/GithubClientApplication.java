package frogermcs.io.githubclient;

import android.app.Application;

import com.nobleworks_software.injection.android.InjectionService;

import javax.inject.Inject;

import frogermcs.io.githubclient.di.GitHubClientInjector;
import frogermcs.io.githubclient.utils.AnalyticsManager;
import timber.log.Timber;

public class GithubClientApplication extends Application
{
    @Inject
    AnalyticsManager analyticsManager;

    @Override
    public void onCreate()
    {
        super.onCreate();

        InjectionService.setInjector(new GitHubClientInjector());

        InjectionService.inject(this);

        if (BuildConfig.DEBUG)
        {
            Timber.plant(new Timber.DebugTree());
        }

        analyticsManager.logAppStartup();
    }
}
