package frogermcs.io.githubclient.views;

import frogermcs.io.githubclient.data.model.User;

public interface SplashView
{
    void showLoading(boolean loading);

    void showRepositoriesListForUser(User user);

    void showValidationError();
}
