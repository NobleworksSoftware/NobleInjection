package frogermcs.io.githubclient.views;

public interface RepositoryDetailsView extends HasUserScope
{
    void setUserName(String userName);
}
