package frogermcs.io.githubclient.views;

import frogermcs.io.githubclient.data.model.User;

public interface HasUserScope
{
    User getUser();
}
