package frogermcs.io.githubclient.views;

import com.google.common.collect.ImmutableList;

import frogermcs.io.githubclient.data.model.Repository;

public interface RepositoryListView extends HasUserScope
{
    void showLoading(boolean loading);

    void setRepositories(ImmutableList<Repository> repositories);
}
