package frogermcs.io.githubclient.di.components;

import javax.inject.Singleton;

import dagger.Component;
import dagger.MembersInjector;
import frogermcs.io.githubclient.GithubClientApplication;
import frogermcs.io.githubclient.data.api.GithubApiModule;
import frogermcs.io.githubclient.di.modules.UserModule;
import frogermcs.io.githubclient.di.modules.ActivityModule;
import frogermcs.io.githubclient.di.modules.AppModule;

/**
 * Created by Miroslaw Stanek on 22.04.15.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                GithubApiModule.class
        }
)
public interface AppComponent {

    UserComponent plus(UserModule userModule);

    ActivityComponent getActivityComponent(ActivityModule activityModule);

    MembersInjector<GithubClientApplication> getAppInjector();
}
