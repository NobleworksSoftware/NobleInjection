package frogermcs.io.githubclient.di.components;

import dagger.MembersInjector;
import dagger.Subcomponent;
import frogermcs.io.githubclient.di.modules.ActivityModule;
import frogermcs.io.githubclient.di.modules.UserActivityModule;
import frogermcs.io.githubclient.di.ActivityScope;
import frogermcs.io.githubclient.ui.activities.RepositoriesListActivity;
import frogermcs.io.githubclient.ui.activities.RepositoryDetailsActivity;
import frogermcs.io.githubclient.ui.views.AvatarView;

@ActivityScope
@Subcomponent(modules = {ActivityModule.class, UserActivityModule.class})
public interface UserActivityComponent
{
    MembersInjector<RepositoriesListActivity> getRepositoriesListActivityInjector();
    MembersInjector<RepositoryDetailsActivity> getRepositoryDetailActivityInjector();
    MembersInjector<AvatarView> getAvatarViewInjector();
}
