package frogermcs.io.githubclient.di.modules;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;
import frogermcs.io.githubclient.di.ActivityScope;
import frogermcs.io.githubclient.presenters.SplashViewPresenter;
import frogermcs.io.githubclient.presenters.impl.SplashViewPresenterImpl;

@Module
public class ActivityModule
{
    private final Activity activity;

    public ActivityModule(Activity activity)
    {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Activity getActivity()
    {
        return activity;
    }

    @Provides
    @ActivityScope
    SplashViewPresenter provideSplashViewPresenter(SplashViewPresenterImpl implementation)
    {
        return implementation;
    }
}
