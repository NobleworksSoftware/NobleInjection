package frogermcs.io.githubclient.di;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.nobleworks_software.injection.GenericComponent;
import com.nobleworks_software.injection.android.InjectionService;

import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import dagger.MembersInjector;
import frogermcs.io.githubclient.di.modules.UserModule;
import frogermcs.io.githubclient.data.model.User;
import frogermcs.io.githubclient.di.components.AppComponent;
import frogermcs.io.githubclient.di.components.DaggerAppComponent;
import frogermcs.io.githubclient.di.components.GenericActivityComponent;
import frogermcs.io.githubclient.di.components.GenericAppComponent;
import frogermcs.io.githubclient.di.components.GenericUserActivityComponent;
import frogermcs.io.githubclient.di.components.UserComponent;
import frogermcs.io.githubclient.di.modules.ActivityModule;
import frogermcs.io.githubclient.di.modules.AppModule;
import frogermcs.io.githubclient.views.HasUserScope;

public class GitHubClientInjector
        implements InjectionService.Injector, Application.ActivityLifecycleCallbacks
{
    GenericComponent<AppComponent> appComponent;

    GenericComponent<AppComponent> getAppComponent(Context context)
    {
        if(appComponent == null)
        {
            Application application = (Application) context.getApplicationContext();

            application.registerActivityLifecycleCallbacks(this);
            appComponent = new GenericAppComponent(DaggerAppComponent.builder()
                    .appModule(new AppModule(application))
                    .build());
        }

        return appComponent;
    }

    private final WeakHashMap<User, UserComponent> userComponents = new WeakHashMap<>();

    private UserComponent getUserComponent(Context context, User user)
    {
        UserComponent component = userComponents.get(user);

        if (component == null)
        {
            component = getAppComponent(context).component.plus(new UserModule(user));

            userComponents.put(user, component);
        }

        return component;
    }

    private Map<Context, GenericComponent> activityComponents = new HashMap<>();

    private GenericComponent getActivityComponent(Activity activity)
    {
        GenericComponent component = activityComponents.get(activity);

        if(component == null)
        {
            if (activity instanceof HasUserScope)
            {
                User user = ((HasUserScope)activity).getUser();

                component = new GenericUserActivityComponent(getUserComponent(activity, user)
                        .getActivityComponent(new ActivityModule(activity)));
            }
            else
            {
                component = new GenericActivityComponent(getAppComponent(activity).component
                        .getActivityComponent(new ActivityModule(activity)));
            }

            activityComponents.put(activity, component);
        }

        return component;
    }

    @Override
    public <T> void inject(Context context, T target)
    {
        GenericComponent<?> component = null;

        if(context instanceof Activity)
        {
            component = getActivityComponent((Activity)context);
        }

        if(component == null)
        {
            component = getAppComponent(context);
        }

        // I am specifically allowing no injector to be defined for a class. If you want an error
        // just replace the following with component.inject(target);
        MembersInjector<? super T> injector = component.getInjector(target);

        if(injector != null)
        {
            injector.injectMembers(target);
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState)
    {
    }

    @Override
    public void onActivityStarted(Activity activity)
    {
    }

    @Override
    public void onActivityResumed(Activity activity)
    {
    }

    @Override
    public void onActivityPaused(Activity activity)
    {
    }

    @Override
    public void onActivityStopped(Activity activity)
    {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState)
    {
    }

    @Override
    public void onActivityDestroyed(Activity activity)
    {
        activityComponents.remove(activity);
    }
}
