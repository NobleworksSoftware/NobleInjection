package frogermcs.io.githubclient.di.modules;

import dagger.Module;
import dagger.Provides;
import frogermcs.io.githubclient.di.ActivityScope;
import frogermcs.io.githubclient.presenters.RepositoriesListViewPresenter;
import frogermcs.io.githubclient.presenters.RepositoryDetailsViewPresenter;
import frogermcs.io.githubclient.presenters.impl.RepositoriesListViewPresenterImpl;
import frogermcs.io.githubclient.presenters.impl.RepositoryDetailsViewPresenterImpl;

@Module
public class UserActivityModule
{
    @Provides
    @ActivityScope
    RepositoriesListViewPresenter provideRepositoryListPresenter(
            RepositoriesListViewPresenterImpl implementation)
    {
        return implementation;
    }

    @Provides
    @ActivityScope
    RepositoryDetailsViewPresenter provideRepositoryDetailsPresenter(
            RepositoryDetailsViewPresenterImpl implementation)
    {
        return implementation;
    }
}
