package frogermcs.io.githubclient.di.components;

import dagger.MembersInjector;
import dagger.Subcomponent;
import frogermcs.io.githubclient.di.modules.ActivityModule;
import frogermcs.io.githubclient.di.ActivityScope;
import frogermcs.io.githubclient.ui.activities.SplashActivity;

@ActivityScope
@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent
{
    MembersInjector<SplashActivity> getSplashActivityInjector();
}
