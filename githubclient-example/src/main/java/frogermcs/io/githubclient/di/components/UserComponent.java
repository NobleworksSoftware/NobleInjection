package frogermcs.io.githubclient.di.components;

import dagger.Subcomponent;
import frogermcs.io.githubclient.di.modules.UserModule;
import frogermcs.io.githubclient.di.UserScope;
import frogermcs.io.githubclient.di.modules.ActivityModule;

@UserScope
@Subcomponent(
        modules = {
                UserModule.class
        }
)
public interface UserComponent
{
    UserActivityComponent getActivityComponent(ActivityModule module);
}
