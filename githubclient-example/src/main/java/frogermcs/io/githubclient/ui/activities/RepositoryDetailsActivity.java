package frogermcs.io.githubclient.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.nobleworks_software.injection.android.InjectionService;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import frogermcs.io.githubclient.R;
import frogermcs.io.githubclient.data.model.Repository;
import frogermcs.io.githubclient.data.model.User;
import frogermcs.io.githubclient.presenters.RepositoryDetailsViewPresenter;
import frogermcs.io.githubclient.views.RepositoryDetailsView;
import frogermcs.io.githubclient.utils.AnalyticsManager;


public class RepositoryDetailsActivity extends AppCompatActivity implements RepositoryDetailsView
{
    private static final String ARG_REPOSITORY = "arg_repository";

    @Bind(R.id.tvRepoName)
    TextView tvRepoName;
    @Bind(R.id.tvRepoDetails)
    TextView tvRepoDetails;
    @Bind(R.id.tvUserName)
    TextView tvUserName;

    @Inject
    AnalyticsManager analyticsManager;
    @Inject
    RepositoryDetailsViewPresenter presenter;

    @InjectExtra("user")
    User user;

    @InjectExtra("repository")
    Repository repository;

    @Override
    public User getUser()
    {
        return user;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Dart.inject(this);

        InjectionService.inject(this);

        setContentView(R.layout.activity_repository_details);
        ButterKnife.bind(this);

        analyticsManager.logScreenView(getClass().getName());

        tvRepoName.setText(repository.name);
        tvRepoDetails.setText(repository.url);

        presenter.bindView(this);
    }

    @Override
    public void setUserName(String userName)
    {
        tvUserName.setText(userName);
    }
}
