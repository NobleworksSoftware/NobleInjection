package frogermcs.io.githubclient.ui.views;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.nobleworks_software.injection.android.InjectionService;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import frogermcs.io.githubclient.data.model.User;

public class AvatarView extends ImageView
{
    @Inject
    User user;

    public AvatarView(Context context)
    {
        super(context);
    }

    public AvatarView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public AvatarView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();

        InjectionService.inject(this);

        String url = user.avatarUrl;

        if(url == null)
        {
            url = "https://help.github.com/assets/images/site/invertocat.png";
        }

        Picasso.with(getContext())
                .load(Uri.parse(url))
                .into(this);
    }
}
