package frogermcs.io.githubclient.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.google.common.collect.ImmutableList;
import com.nobleworks_software.injection.android.InjectionService;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import frogermcs.io.githubclient.R;
import frogermcs.io.githubclient.activities.Henson;
import frogermcs.io.githubclient.data.model.Repository;
import frogermcs.io.githubclient.data.model.User;
import frogermcs.io.githubclient.presenters.RepositoriesListViewPresenter;
import frogermcs.io.githubclient.ui.adapter.RepositoriesListAdapter;
import frogermcs.io.githubclient.views.RepositoryListView;
import frogermcs.io.githubclient.utils.AnalyticsManager;


public class RepositoriesListActivity extends AppCompatActivity implements RepositoryListView
{
    @Bind(R.id.lvRepositories)
    ListView lvRepositories;

    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;

    @InjectExtra("user")
    User user;

    @Inject
    RepositoriesListViewPresenter presenter;

    @Inject
    AnalyticsManager analyticsManager;

    private RepositoriesListAdapter repositoriesListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Dart.inject(this);
        InjectionService.inject(this);

        setContentView(R.layout.activity_repositories_list);

        ButterKnife.bind(this);

        repositoriesListAdapter = new RepositoriesListAdapter(this, new ArrayList<Repository>());
        lvRepositories.setAdapter(repositoriesListAdapter);

        presenter.bindView(this);

        presenter.loadRepositories();
    }

    @Override
    public User getUser()
    {
        return user;
    }

    public void showLoading(boolean loading)
    {
        lvRepositories.setVisibility(loading ? View.GONE : View.VISIBLE);
        pbLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    public void setRepositories(ImmutableList<Repository> repositories)
    {
        repositoriesListAdapter.clear();
        repositoriesListAdapter.addAll(repositories);
    }

    @OnItemClick(R.id.lvRepositories)
    public void onRepositoryClick(int position)
    {
        Repository repository = repositoriesListAdapter.getItem(position);

        startActivity(Henson.with(this).gotoRepositoryDetailsActivity()
                .repository(repository).user(user).build());
    }
}
