/*
 * Copyright (C) 2015, Dale King
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("InjectionService")
package com.nobleworks_software.injection.android.kotlin

import android.annotation.TargetApi
import android.app.Fragment
import android.content.Context
import android.os.Build
import android.preference.Preference
import android.view.View
import android.support.v4.app.Fragment as SupportFragment

/**
 * Interface to the code to handle injection into the given target in the given Android
 * Context.
 */
public interface Injector
{
    /** Perform injections on the given target based on the given Android context
     *
     * @param context The Android context that can be used for determining how to do the injection.
     * @param target The object to be injected.
     * @param <T> The type of the object to be injected.
     */
    public fun <T> inject(context: Context, target: T)
}

/**
 * The currently registered injector for the system.
 */
private var injector: Injector? = null

/**
 * Sets this injector as the current injector to use for injection
 */
public fun Injector.setAsInjector()
{
    injector = this;
}

/**
 * Perform injections into the given android context (e.g. Application, Activity,
 * or Service)
 */
fun Context.inject()
{
    this inject this
}

/**
 * Perform injections into the given non-support library fragment
 */
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public fun Fragment.inject()
{
    activity inject this
}

/**
 * Perform injections into the given support library fragment
 */
public fun SupportFragment.inject()
{
    activity inject this
}

/**
 * Perform injections into the given view
 */
public fun View.inject()
{
    context inject this
}

/**
 * Perform injections into the given preference object
 */
public fun Preference.inject()
{
    context inject this
}

/**
 * Perform injections into the given target using the given context
 *
 * @param target The object on which injection is to be performed.
 * @throws IllegalStateException If no injector is registered.
 */
public infix fun Context.inject(target: Any)
{
    injector?.inject(this, target)
            ?: throw IllegalStateException("No injector has been registered")
}
