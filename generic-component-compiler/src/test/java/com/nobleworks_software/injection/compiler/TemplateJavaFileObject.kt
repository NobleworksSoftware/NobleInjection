/*
 * Copyright (C) 2015, Dale King
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nobleworks_software.injection.compiler

import com.google.common.io.ByteSource
import java.io.IOException
import java.io.InputStream
import java.net.URI
import java.net.URL
import java.nio.charset.Charset
import javax.tools.JavaFileObject
import javax.tools.SimpleJavaFileObject

public class TemplateJavaFileObject(url: URL, variables: Map<String, String>)
    : SimpleJavaFileObject(URI.create(url.toString()), JavaFileObject.Kind.SOURCE)
{
    val source = object : ByteSource()
    {
        override fun openStream(): InputStream
            = VariableSubstitutionInputStream(url.openStream(), variables)
    }

    @Throws(IOException::class)
    override fun getCharContent(ignoreEncodingErrors: Boolean)
            = source.asCharSource(Charset.defaultCharset()).read()

    @Throws(IOException::class)
    override fun openInputStream() = source.openStream()

    @Throws(IOException::class)
    override fun openReader(ignoreEncodingErrors: Boolean)
            = source.asCharSource(Charset.defaultCharset()).openStream()
}
