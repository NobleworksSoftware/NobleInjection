/*
 * Copyright (C) 2015, Dale King
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nobleworks_software.injection.compiler

import com.google.common.io.Resources
import com.google.common.truth.Truth.assert_
import com.google.testing.compile.JavaSourcesSubjectFactory.javaSources
import org.junit.Test
import javax.tools.JavaFileObject

public abstract class AbstractComponentProcessorTest
{
    private fun getSource(resourceName: String) : JavaFileObject
            = TemplateJavaFileObject(Resources.getResource("$resourceName.java"),
            mapOf("componentName" to getComponentType().canonicalName))

    private fun getSources(directory: String, vararg names: String) : List<JavaFileObject>
            = names.map { getSource("$directory/$it") }

    protected abstract fun getComponentType(): Class<out Annotation>

    @Test public fun simpleComponent()
    {
        val sourceFiles = getSources("simpleComponentTest",
                "SomeInjectableType", "OtherInjectableType", "SimpleSubComponent", "SimpleComponent")

        val expectedSource = getSource("simpleComponentTest/GenericSimpleComponent")

        assert_().about(javaSources())
                .that(sourceFiles)
                .processedWith(GenericComponentProcessor())
                .compilesWithoutError().and().generatesSources(expectedSource)
    }

    @Test public fun simpleComponentWithNesting()
    {
        val nestedTypesFile = getSource("simpleComponentWithNestingTest/OuterType")

        val generatedSource = getSource("simpleComponentWithNestingTest/GenericOuterType_SimpleComponent")

        assert_().about(javaSources())
                .that(listOf(nestedTypesFile))
                .processedWith(GenericComponentProcessor())
                .compilesWithoutError().and().generatesSources(generatedSource)
    }

    @Test public fun membersInjectionTypePrecedence()
    {
        val sourceFiles = getSources("membersInjectionTypePrecedenceTest",
                "I", "I2", "A", "B", "C", "D", "E", "SimpleComponent")

        val generatedSource = getSource("membersInjectionTypePrecedenceTest/GenericSimpleComponent")

        assert_().about(javaSources())
                .that(sourceFiles)
                .processedWith(GenericComponentProcessor())
                .compilesWithoutError().and().generatesSources(generatedSource)
    }

    @Test public fun membersInjectionTypePrecedence2() {
        val sourceFiles = getSources("membersInjectionTypePrecedence2",
                "D", "A", "B", "C", "SimpleComponent")

        val generatedSource = getSource("membersInjectionTypePrecedence2/GenericSimpleComponent")

        assert_().about(javaSources())
                .that(sourceFiles)
                .processedWith(GenericComponentProcessor())
                .compilesWithoutError().and().generatesSources(generatedSource)
    }

    @Test public fun membersInjector() {
        val sourceFiles = getSources("membersInjectorTest",
                "A", "B", "SimpleComponent")

        val generatedSource = getSource("membersInjectorTest/GenericSimpleComponent")

        assert_().about(javaSources())
                .that(sourceFiles)
                .processedWith(GenericComponentProcessor())
                .compilesWithoutError().and().generatesSources(generatedSource)
    }
}
