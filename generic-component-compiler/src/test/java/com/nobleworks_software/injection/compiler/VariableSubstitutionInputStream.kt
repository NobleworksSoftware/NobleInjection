/*
 * Copyright (C) 2015, Dale King
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nobleworks_software.injection.compiler

import java.io.IOException
import java.io.InputStream
import java.io.PushbackInputStream

public class VariableSubstitutionInputStream(input: InputStream,
                                 private val variables: Map<String, String>) : InputStream()
{
    private val pushbackInputStream: PushbackInputStream = PushbackInputStream(input)
    private var currentValue: ByteArray? = null
    private var index = 0

    @Throws(IOException::class)
    override fun read(): Int
    {
        var result: Int

        if (currentValue != null && index < currentValue!!.size())
        {
            result = currentValue!![index++].toInt()
        }
        else
        {
            currentValue = null
            result = pushbackInputStream.read()

            when (result) {
                '\\'.toInt() ->
                {
                    result = pushbackInputStream.read()

                    if (result != '$'.toInt()) {
                        pushbackInputStream.unread(result)
                        result = '\\'.toInt()
                    }
                }

                '$'.toInt() ->
                {
                    result = pushbackInputStream.read()

                    if (result != '{'.toInt()) {
                        pushbackInputStream.unread(result)
                        result = '$'.toInt()
                    }
                    else {
                        val buffer = StringBuilder()

                        result = pushbackInputStream.read()

                        while (result != '}'.toInt())
                        {
                            if (result == -1)
                            {
                                throw IOException("Unexpected end of stream: missing '}'")
                            }

                            buffer.append(result.toChar())

                            result = pushbackInputStream.read()
                        }

                        val key = buffer.toString()
                        var value: String = variables.get(key)?: "\${${key}}"

                        currentValue = value.toByteArray()
                        index = 0

                        result = read()
                    }
                }
            }
        }

        return result
    }

    override fun available() = 0

    @Throws(IOException::class)
    override fun close() = pushbackInputStream.close()
}
