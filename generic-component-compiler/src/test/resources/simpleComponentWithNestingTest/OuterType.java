package simpleComponentWithNestingTest;

import ${componentName};
import javax.inject.Inject;

final class OuterType
{
    final static class A
    {
        @Inject A() {}
    }

    final static class B
    {
        @Inject A a;
    }

    @${componentName} interface SimpleComponent
    {
        A a();
        void inject(B b);
    }
}
