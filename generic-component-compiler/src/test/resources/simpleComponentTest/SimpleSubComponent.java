package simpleComponentTest;

import dagger.Subcomponent;

@Subcomponent
interface SimpleSubcomponent
{
    OtherInjectableType otherInjectableType();
}
