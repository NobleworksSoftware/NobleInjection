package simpleComponentTest;

import javax.inject.Inject;

final class OtherInjectableType
{
    @Inject OtherInjectableType() {}
}
