package membersInjectionTypePrecedence2;

import com.nobleworks_software.injection.ClassInjectorMap;
import com.nobleworks_software.injection.GenericComponent;
import dagger.MembersInjector;
import java.lang.IllegalArgumentException;
import javax.annotation.Generated;

@Generated("com.nobleworks_software.injection.compiler.GenericComponentProcessor")
public final class GenericSimpleComponent extends GenericComponent<SimpleComponent>
{
    public GenericSimpleComponent(SimpleComponent component)
    {
        super(component);
    }

    protected ClassInjectorMap buildInjectorMap()
    {
        ClassInjectorMap map = new ClassInjectorMap(5);
        map.put(A.class, new SimpleInjector(0);
        map.put(B.class, new SimpleInjector(1);
        map.put(C.class, new SimpleInjector(2);
        return map;
    }

    private class SimpleInjector<T> implements MembersInjector<T>
    {
        private final int index;

        private SimpleInjector(int index)
        {
            this.index = index;
        }

        public void injectMembers(T instance)
        {
            switch (index)
            {
                case 0:
                    component.inject((A) instance);
                    break;
                case 1:
                    component.inject((B) instance);
                    break;
                case 2:
                    component.inject((C) instance);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid Index!");
            }
        }
    }
}
