package membersInjectionTypePrecedence2;

import ${componentName};

@${componentName}
interface SimpleComponent {
    void inject(B b);
    void inject(A a);
    void inject(C c);
}
