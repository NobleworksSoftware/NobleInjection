package membersInjectionTypePrecedence2;

import javax.inject.Inject;

final class D
{
    @Inject D() {}
}
