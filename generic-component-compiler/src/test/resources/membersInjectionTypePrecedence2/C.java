package membersInjectionTypePrecedence2;

import javax.inject.Inject;

final class C extends A
{
    @Inject B b;
}
