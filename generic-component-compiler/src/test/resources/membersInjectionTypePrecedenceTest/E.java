package membersInjectionTypePrecedenceTest;

import javax.inject.Inject;

final class E
{
    @Inject E() {}
}
