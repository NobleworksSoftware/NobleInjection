package membersInjectionTypePrecedenceTest;

import javax.inject.Inject;

final class D implements I2
{
    @Inject public void setC(C c) {}
    @Inject public void setE(E e) {}
}
