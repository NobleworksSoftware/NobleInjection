package membersInjectionTypePrecedenceTest;

import javax.inject.Inject;
import javax.inject.Provider;

final class C extends B
{
    @Inject Provider<I> iProvider;
}
