package membersInjectionTypePrecedenceTest;

import ${componentName};

@${componentName}
interface SimpleComponent
{
    void inject(I i);
    void inject(I2 i1);
    void inject(A a);
    void inject(B b);
    void inject(C c);
    void inject(D d);
}
