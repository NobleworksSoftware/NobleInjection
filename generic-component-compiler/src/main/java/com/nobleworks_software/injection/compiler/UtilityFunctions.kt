/*
 * Copyright (C) 2015, Dale King
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nobleworks_software.injection.compiler

import com.google.auto.common.AnnotationMirrors
import com.google.auto.common.MoreElements
import com.google.auto.common.MoreTypes
import com.google.auto.common.Visibility
import dagger.MembersInjector
import dagger.Subcomponent
import java.math.BigInteger
import javax.inject.Provider
import javax.inject.Qualifier
import javax.lang.model.element.Element
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.PackageElement
import javax.lang.model.element.TypeElement
import javax.lang.model.type.TypeKind
import javax.lang.model.type.TypeMirror
import javax.lang.model.util.Types

fun Element.isVisibleFrom(from: PackageElement): Boolean =
        when (Visibility.effectiveVisibilityOfElement(this))
        {
            Visibility.PUBLIC -> true
            Visibility.PROTECTED, Visibility.DEFAULT -> MoreElements.getPackage(this) == from
            Visibility.PRIVATE -> false
            else -> throw AssertionError()
        }

internal fun determineHashMapSize(itemCount: Int)
        = BigInteger((itemCount * 10 / 7).toString()).nextProbablePrime().toString().toInt()

internal fun Element.toType() = MoreElements.asType(this)

internal fun Element.asDeclaredType() = MoreTypes.asDeclared(this.asType())

internal inline fun <reified T : Any> TypeMirror.isOfType() = MoreTypes.isTypeOf(T::class.java, this)

internal inline fun <reified T : Annotation> TypeMirror.hasAnnotation(types: Types) : Boolean
        = MoreElements.getAnnotationMirror(types.asElement(this), T::class.java).isPresent()

internal fun TypeMirror.isOfKind(kind: TypeKind) = this.kind === kind

internal fun TypeMirror.isVoid() = this.kind === TypeKind.VOID

internal fun TypeMirror.isDeclared() = this.kind === TypeKind.DECLARED

internal fun Element.hasQualifier() =
        !AnnotationMirrors.getAnnotatedAnnotations(this, Qualifier::class.java).isEmpty()

fun ExecutableElement.toComponentMethod(types: Types, element: TypeElement): ComponentMethodDescriptor?
{
    val resolvedComponentMethod = MoreTypes.asExecutable(types.asMemberOf(element.asDeclaredType(), this))

    val returnType = resolvedComponentMethod.returnType
    val parameterTypes = resolvedComponentMethod.parameterTypes

    if (returnType.isDeclared())
    {
        if (returnType.isOfType<Provider<Any>>() || returnType.isOfType<Lazy<Any>>())
        {
            return ComponentMethodDescriptor(ComponentMethodKind.PROVIDER_OR_LAZY,
                    MoreTypes.asDeclared(returnType).typeArguments.first(), this)
        }
        else if (returnType.isOfType<MembersInjector<Any>>())
        {
            return ComponentMethodDescriptor(ComponentMethodKind.MEMBERS_INJECTOR,
                    MoreTypes.asDeclared(returnType).typeArguments.first(), this)
        }
        else if (returnType.hasAnnotation<Subcomponent>(types))
        {
            // Ignore subcomponent methods
            return null
        }
    }

    if (parameterTypes.isEmpty() && !returnType.isOfKind(TypeKind.VOID))
    {
        return ComponentMethodDescriptor(ComponentMethodKind.SIMPLE_PROVISION, returnType,
                this)
    }

    if (parameterTypes.size == 1)
    {
        val parameter = parameterTypes[0]
        if(parameter.isDeclared()
                && (returnType.isVoid() || types.isSameType(returnType, parameter)))
        {
            return ComponentMethodDescriptor(ComponentMethodKind.SIMPLE_MEMBERS_INJECTION,
                    parameter, this)
        }
    }

    return null
}

